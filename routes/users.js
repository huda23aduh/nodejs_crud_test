var express = require('../node_modules/express')
var app = express()

//var html = template.render({ clickHandler:"func1();" })
 
// SHOW LIST OF USERS
app.get('/', function(req, res, next) {
    req.getConnection(function(error, conn) {
        conn.query('SELECT * from 12_2017 where IS_DELETED = 0 ',function(err, rows, fields) {
            //if(err) throw err
            if (err) {
                req.flash('error', err)
                res.render('user/list', {
                    title: 'User List', 
                    data: ''
                })
            } else {
                // render to views/user/list.ejs template file
                res.render('user/list', {
                    title: 'User List', 
                    clickHandler:"func1();",
                    data: rows
                })
            }
        })
    })
})
//========================================================================================


// SHOW SEARCH USER FORM
function findRegencies(req, res, next) {
        req.getConnection(function(error, conn) {
            conn.query('SELECT DISTINCT(CITY) FROM 12_2017 ORDER BY CITY ASC',function(err, rows, fields) {
                //if(err) throw err
                if (err) {
                    res.render('incorrect_regency'); /* Render the error page. */      
                } else {
                    req.regencies = rows;
                    return next();
                }
            })
        })

}

function findCompetences(req, res, next) {
        req.getConnection(function(error, conn) {
            conn.query('SELECT DISTINCT COMPETENCE FROM 12_2017 ORDER BY COMPETENCE ASC',function(err, rows, fields) {
                //if(err) throw err
                if (err) {
                    res.render('incorrect_competence'); /* Render the error page. */      
                } else {
                    req.competences = rows;
                    return next();
                }
            })
        })
}

function renderStudentsPage(req, res) {
    res.render('user/search', {
        title: 'Search Form',
        the_regencies: req.regencies,
        the_competences: req.competences
    });
}

app.get('/search', findRegencies, findCompetences,  renderStudentsPage);

//========================================================================================



// SEARCH USER POST ACTION
app.post('/search', function(req, res, next){    
    //req.assert('name_input', 'Name is required').notEmpty()           //Validate name
    // req.assert('age', 'Age is required').notEmpty()             //Validate age
    // req.assert('email', 'A valid email is required').isEmail()  //Validate email
 
    var errors = req.validationErrors()
    
    if( !errors ) {   //No errors were found.  Passed Validation!
        
        /********************************************
         * Express-validator module
         
        req.body.comment = 'a <span>comment</span>';
        req.body.username = '   a user    ';
 
        req.sanitize('comment').escape(); // returns 'a &lt;span&gt;comment&lt;/span&gt;'
        req.sanitize('username').trim(); // returns 'a user'
        ********************************************/
        var user = {
            name: req.sanitize('name_input').escape().trim(),
            str_number: req.sanitize('str_number_input').escape().trim(),
            competence: req.sanitize('competence_value_input').escape().trim(),
            city_district_1: req.sanitize('city_district_input_1').escape().trim(),
            city_district_2: req.sanitize('city_district_input_2').escape().trim(),
            city_district_3: req.sanitize('city_district_input_3').escape().trim()
        }

        var is_deleted_value = 0;
        var name_value = '%' + user.name + '%';
        var competence_value = '%' + user.competence + '%';
        var city_district_value_1 = '%' + user.city_district_1 + '%';
        var city_district_value_2 = '%' + user.city_district_2 + '%';
        var city_district_value_3 = '%' + user.city_district_3 + '%';
        var str_number_value = '%' + user.str_number + '%';

        if(user.competence == 'Dokter Umum')
            competence_value = '%' + 'Dokter';



        req.getConnection(function(error, conn) {
            var sql = 'SELECT * FROM 12_2017 WHERE IS_DELETED = ? AND NAME LIKE ? AND COMPETENCE LIKE ? AND STR_NUMBER LIKE ? AND CITY LIKE ? OR CITY LIKE ? OR CITY LIKE ? ORDER BY NAME ASC';
            conn.query(sql, [is_deleted_value, name_value , competence_value , str_number_value ,  city_district_value_1, city_district_value_2, city_district_value_3],function(err, rows, fields) {
                console.log('this.sql', this.sql); //command/query
                //if(err) throw err
                if (err) {
                    req.flash('error', err)
                    res.render('user/list', {
                        title: 'Result List', 
                        data: ''
                    })
                } else {
                    // render to views/user/list.ejs template file
                    res.render('user/list', {
                        title: 'Result List', 
                        data: rows
                    })
                }
            })
        })
    }
    else {   //Display errors to user
        var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + '<br>'
        })                
        req.flash('error', error_msg)        
        
        /**
         * Using req.body.name 
         * because req.param('name') is deprecated
         */ 
        res.render('user/search', { 
            title: 'SEARCH Form',
            name: req.body.name,
            // age: req.body.age,
            // email: req.body.email
            data:''
        })
    }
})
//========================================================================================
 
// SHOW ADD USER FORM
app.get('/add', function(req, res, next){    
    // render to views/user/add.ejs
    res.render('user/add', {
        title: 'Add New User',
        name: '',
        age: '',
        email: ''        
    })
})
//========================================================================================
 
// ADD NEW USER POST ACTION
app.post('/add', function(req, res, next){    
    req.assert('name', 'Name is required').notEmpty()           //Validate name
    req.assert('age', 'Age is required').notEmpty()             //Validate age
    req.assert('email', 'A valid email is required').isEmail()  //Validate email
 
    var errors = req.validationErrors()
    
    if( !errors ) {   //No errors were found.  Passed Validation!
        
        /********************************************
         * Express-validator module
         
        req.body.comment = 'a <span>comment</span>';
        req.body.username = '   a user    ';
 
        req.sanitize('comment').escape(); // returns 'a &lt;span&gt;comment&lt;/span&gt;'
        req.sanitize('username').trim(); // returns 'a user'
        ********************************************/
        var user = {
            name: req.sanitize('name').escape().trim(),
            age: req.sanitize('age').escape().trim(),
            email: req.sanitize('email').escape().trim()
        }
        
        req.getConnection(function(error, conn) {
            conn.query('INSERT INTO users SET ?', user, function(err, result) {
                //if(err) throw err
                if (err) {
                    req.flash('error', err)
                    
                    // render to views/user/add.ejs
                    res.render('user/add', {
                        title: 'Add New User',
                        name: user.name,
                        age: user.age,
                        email: user.email                    
                    })
                } else {                
                    req.flash('success', 'Data added successfully!')
                    
                    // render to views/user/add.ejs
                    res.render('user/add', {
                        title: 'Add New User',
                        name: '',
                        age: '',
                        email: ''                    
                    })
                }
            })
        })
    }
    else {   //Display errors to user
        var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + '<br>'
        })                
        req.flash('error', error_msg)        
        
        /**
         * Using req.body.name 
         * because req.param('name') is deprecated
         */ 
        res.render('user/add', { 
            title: 'Add New User',
            name: req.body.name,
            age: req.body.age,
            email: req.body.email
        })
    }
})
//========================================================================================

// SHOW EDIT USER FORM
app.get('/edit/(:id)', function(req, res, next){
    req.getConnection(function(error, conn) {
        conn.query('SELECT * FROM users WHERE id = ' + req.params.id, function(err, rows, fields) {
            if(err) throw err
            
            // if user not found
            if (rows.length <= 0) {
                req.flash('error', 'User not found with id = ' + req.params.id)
                res.redirect('/users')
            }
            else { // if user found
                // render to views/user/edit.ejs template file
                res.render('user/edit', {
                    title: 'Edit User', 
                    //data: rows[0],
                    id: rows[0].id,
                    name: rows[0].name,
                    age: rows[0].age,
                    email: rows[0].email                    
                })
            }            
        })
    })
})
//========================================================================================
 
// EDIT USER POST ACTION
app.put('/edit/(:id)', function(req, res, next) {
    req.assert('name', 'Name is required').notEmpty()           //Validate name
    req.assert('age', 'Age is required').notEmpty()             //Validate age
    req.assert('email', 'A valid email is required').isEmail()  //Validate email
 
    var errors = req.validationErrors()
    
    if( !errors ) {   //No errors were found.  Passed Validation!
        
        /********************************************
         * Express-validator module
         
        req.body.comment = 'a <span>comment</span>';
        req.body.username = '   a user    ';
 
        req.sanitize('comment').escape(); // returns 'a &lt;span&gt;comment&lt;/span&gt;'
        req.sanitize('username').trim(); // returns 'a user'
        ********************************************/
        var user = {
            name: req.sanitize('name').escape().trim(),
            age: req.sanitize('age').escape().trim(),
            email: req.sanitize('email').escape().trim()
        }
        
        req.getConnection(function(error, conn) {
            conn.query('UPDATE users SET ? WHERE id = ' + req.params.id, user, function(err, result) {
                //if(err) throw err
                if (err) {
                    req.flash('error', err)
                    
                    // render to views/user/add.ejs
                    res.render('user/edit', {
                        title: 'Edit User',
                        id: req.params.id,
                        name: req.body.name,
                        age: req.body.age,
                        email: req.body.email
                    })
                } else {
                    req.flash('success', 'Data updated successfully!')
                    
                    // render to views/user/add.ejs
                    res.render('user/edit', {
                        title: 'Edit User',
                        id: req.params.id,
                        name: req.body.name,
                        age: req.body.age,
                        email: req.body.email
                    })
                }
            })
        })
    }
    else {   //Display errors to user
        var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + '<br>'
        })
        req.flash('error', error_msg)
        
        /**
         * Using req.body.name 
         * because req.param('name') is deprecated
         */ 
        res.render('user/edit', { 
            title: 'Edit User',            
            id: req.params.id, 
            name: req.body.name,
            age: req.body.age,
            email: req.body.email
        })
    }
})
//========================================================================================
 
// DELETE USER
app.delete('/delete/(:id)', function(req, res, next) {
    var user = { id: req.params.id }
    
    req.getConnection(function(error, conn) {
        conn.query('DELETE FROM users WHERE id = ' + req.params.id, user, function(err, result) {
            //if(err) throw err
            if (err) {
                req.flash('error', err)
                // redirect to users list page
                res.redirect('/users')
            } else {
                req.flash('success', 'User deleted successfully! id = ' + req.params.id)
                // redirect to users list page
                res.redirect('/users')
            }
        })
    })
})
 
module.exports = app